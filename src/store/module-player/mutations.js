export function SET_TRACK (state, track) {
  state.track = track
}

export function SET_AUDIO (state, url) {
  if (state.audio !== null) {
    state.audio.pause()
    state.playbackPauseState = false
  }
  state.audio = new Audio(url)
  state.audio.play()
  state.playbackPauseState = false
  state.audio.volume = state.playbackVolume
}

export function TOGGLE_PLAYBACK_STATE (state) {
  if (state.audio.paused) {
    state.audio.play()
    state.playbackPauseState = false
  } else {
    state.audio.pause()
    state.playbackPauseState = true
  }
}

export function SET_TRACK_CURRENT_TIME (state, currentTime) {
  state.trackCurrentTime = currentTime
}

export function SET_TRACK_DURATION (state, duration) {
  state.trackDuration = duration
}

export function SET_PLAYBACK_TIME (state, newTime) {
  if (state.audio !== null) {
    state.audio.currentTime = newTime
  }
}

export function SET_VOLUME (state, newVolume) {
  if (state.audio !== null) {
    state.audio.volume = newVolume
    state.playbackVolume = newVolume
  }
}
