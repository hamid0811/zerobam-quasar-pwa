export function SET_TRACK ({ state, commit }, track) {
  commit('SET_TRACK', track)
  commit('SET_AUDIO', track.url)
  state.audio.addEventListener('timeupdate', (event) => {
    commit('SET_TRACK_CURRENT_TIME', state.audio.currentTime)
    commit('SET_TRACK_DURATION', state.audio.duration)
  })
}

export function TOGGLE_PLAYBACK_STATE ({ commit }) {
  commit('TOGGLE_PLAYBACK_STATE')
}

export function SET_PLAYBACK_TIME ({ commit }, newTime) {
  commit('SET_PLAYBACK_TIME', newTime)
}

export function SET_VOLUME ({ commit }, newVolume) {
  commit('SET_VOLUME', newVolume)
}
