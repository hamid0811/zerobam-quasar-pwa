export default {
  track: {},
  audio: null,
  trackCurrentTime: 0,
  trackDuration: 0,
  playbackPauseState: false,
  playbackVolume: 1
}
