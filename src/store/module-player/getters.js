export function PLAYING_TRACK (state) {
  if (Object.keys(state.track).length) {
    return state.track
  } else {
    return null
  }
}

export function duration (state) {
  if (state.audio !== null) {
    return Math.round(state.trackDuration)
  } else {
    return 100
  }
}

export function currentTime (state) {
  if (state.audio !== null) {
    return Math.round(state.trackCurrentTime)
  } else {
    return 0
  }
}

export function trackDurationInString (state, getters) {
  if (state.audio !== null) {
    var mins = Math.floor(getters.duration / 60)
    var secs = Math.floor(getters.duration % 60)
    if (secs < 10) {
      secs = '0' + String(secs)
    }
    if (isNaN(mins) || isNaN(secs)) {
      return '0:00'
    } else {
      var trackDuration = mins + ':' + secs
      if (trackDuration === 'undefined') {
        return '0:00'
      } else {
        return trackDuration
      }
    }
  } else {
    return '0:00'
  }
}

export function trackCurrentTimeInString (state, getters) {
  if (state.audio !== null) {
    var mins = Math.floor(getters.currentTime / 60)
    var secs = Math.floor(getters.currentTime % 60)

    if (secs < 10) {
      secs = '0' + String(secs)
    }
    var currentTime = mins + ':' + secs
    return currentTime
  } else {
    return '0:00'
  }
}

export function lyrics (state) {
  if (state.track !== null && state.track.lyrics && state.track.lyrics.length > 0) {
    return state.track.lyrics[0].lyric
  } else {
    return null
  }
}

export function playbackPauseState (state) {
  return state.playbackPauseState
}

export function playbackVolume (state) {
  return state.playbackVolume
}
