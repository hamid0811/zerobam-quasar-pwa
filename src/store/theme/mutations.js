export function SET_BACKGROUND (state, newColor) {
  state.background = newColor
}

export function SET_DEFAULT_BACKGROUND_COLOR (state) {
  state.background = state.defaultBackgroundColor
}
