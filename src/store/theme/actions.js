export function SET_BACKGROUND ({ state, commit }, backgroundColor) {
  commit('SET_BACKGROUND', backgroundColor)
}

export function SET_DEFAULT_BACKGROUND_COLOR ({ state, commit }) {
  commit('SET_DEFAULT_BACKGROUND_COLOR')
}
