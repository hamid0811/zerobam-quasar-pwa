import { LocalStorage } from 'quasar'

export default ({ router, store, Vue }) => {
  router.beforeEach((to, from, next) => {
    let value = LocalStorage.getItem('user')
    if (to.path === '/auth/register') {
      next()
    } else {
      if (value === null && to.path !== '/auth') {
        next('/auth')
      } else if (value !== null && to.path === '/auth') {
        next('/')
      } else {
        next()
      }
    }
  })
}
