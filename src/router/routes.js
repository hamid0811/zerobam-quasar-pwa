
const routes = [
  {
    path: '/',
    component: () => import('layouts/Master.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/search', component: () => import('pages/search/index.vue') },
      { path: '/user', component: () => import('pages/user/User.vue') },
      { path: '/artist/:id', component: () => import('pages/artist/Artist.vue'), props: true },
      { path: '/collection/:id', component: () => import('pages/collection/Collection.vue'), props: true }
    ]
  },
  {
    path: '/auth',
    component: () => import('layouts/Authuntication.vue'),
    children: [
      { path: '', component: () => import('pages/auth/Athunticate.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
